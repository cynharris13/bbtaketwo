package com.example.bbtaketwo.view.characterscreen

import com.example.bbtaketwo.model.entity.CharacterPage

/**
 * Character screen state contains the status of the screen.
 *
 * @property isLoading
 * @property characters
 * @property error
 * @property selectedCharacter
 * @constructor Create empty Character screen state
 */
data class CharacterScreenState(
    val isLoading: Boolean = false,
    val characters: List<CharacterPage> = emptyList(),
    val error: String = "",
    val selectedCharacter: CharacterPage = CharacterPage()
)
/*
 val age: String,
    val firstEpisode: String,
    val gender: String,
    val id: Int,
    val image: String,
    val name: String,
    val occupation: String,
    val relatives: List<RelativeDTO>,
    val url: String,
    val voicedBy: String,
    val wikiUrl: String
 */
