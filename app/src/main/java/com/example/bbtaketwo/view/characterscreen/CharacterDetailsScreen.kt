package com.example.bbtaketwo.view.characterscreen

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.example.bbtaketwo.model.entity.CharacterPage
import com.example.bbtaketwo.model.entity.Relative

private const val RIGHT_WEIGHT = 5f
private const val LEFT_WEIGHT = 1f

@Composable
fun CharacterDetailsScreen(
    character: CharacterPage
) {
    Column() {
        Row() {
            Column(modifier = Modifier.weight(LEFT_WEIGHT)) {
                AsyncImage(model = character.image, contentDescription = "Profile pic")
            }
            Column(modifier = Modifier.weight(RIGHT_WEIGHT)) {
                Text(text = character.name, fontWeight = FontWeight.Bold, fontSize = 40.sp)
            }
        }
        Row() {
            Text(text = "Age: ${character.age}", fontSize = 20.sp)
        }
        Row() {
            Text(text = "Gender: ${character.gender}", fontSize = 20.sp)
        }
        Row() {
            Text(text = "First Episode: ${character.firstEpisode}", fontSize = 20.sp)
        }
        Row() {
            Text(text = "Occupation: ${character.occupation}", fontSize = 20.sp)
        }
        Row() {
            Text(text = "Voiced by: ${character.voicedBy}", fontSize = 20.sp)
        }
        Row() {
            Text(text = "Urls: ${character.url}\n${character.wikiUrl}")
        }
        Row(modifier = Modifier.background(Color.Cyan)) {
            Text(text = displayRelatives(character.relatives))
        }
    }
}

/**
 * Display relatives.
 *
 * @param relatives
 * @return
 */
fun displayRelatives(relatives: List<Relative>): String {
    var text = "Relatives: "
    for (relative in relatives) {
        text += "\n\nName: ${relative.name}\nRelationship: ${relative.relationship}"
        text += "\nUrls: ${relative.url}\n${relative.wikiUrl}"
    }
    return text
}
