package com.example.bbtaketwo.model.remote.response

import com.example.bbtaketwo.model.dtos.CharacterPageDTO

/**
 * Character response from json.
 *
 * @constructor Create empty Character response from json
 */
class CharacterResponseFromJson : ArrayList<CharacterPageDTO>()
