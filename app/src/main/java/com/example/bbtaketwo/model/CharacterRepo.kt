package com.example.bbtaketwo.model

import com.example.bbtaketwo.model.entity.CharacterPage
import com.example.bbtaketwo.model.mapper.character.CharacterMapper
import com.example.bbtaketwo.model.remote.CharacterAPI
import com.example.bbtaketwo.model.remote.response.CharacterResponseFromJson
import com.example.bbtaketwo.model.remote.response.NetworkResponse
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

/**
 * Character repository retrieves [CharacterPage] instances from [CharacterAPI].
 *
 * @property mapper maps dtos to [CharacterPage]
 * @property api retrieves the data
 * @property dispatcher selects the thread
 * @constructor Create empty Character repo
 */
class CharacterRepo(
    private val mapper: CharacterMapper,
    private val api: CharacterAPI,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    /**
     * Get character pages.
     *
     * @return a network response
     */
    suspend fun getCharacterPages(limit: Int): NetworkResponse<*> = withContext(dispatcher) {
        val characterResponse: Response<CharacterResponseFromJson> = api.getCharacters(limit).execute()

        return@withContext if (characterResponse.isSuccessful) {
            val pageResponse = characterResponse.body() ?: CharacterResponseFromJson()
            val characterList: List<CharacterPage> = pageResponse.map { mapper(it) }
            NetworkResponse.SuccessfulCharacter(characterList)
        } else {
            NetworkResponse.Error(characterResponse.message())
        }
    }
}
