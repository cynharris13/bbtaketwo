package com.example.bbtaketwo.model.entity

/**
 * Relative listed on a given [CharacterPage].
 *
 * @property name
 * @property relationship
 * @property url
 * @property wikiUrl
 * @constructor Create empty Relative
 */
data class Relative(
    val name: String,
    val relationship: String,
    val url: String,
    val wikiUrl: String
)
