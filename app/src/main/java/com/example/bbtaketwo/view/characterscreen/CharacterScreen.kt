package com.example.bbtaketwo.view.characterscreen

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.example.bbtaketwo.model.entity.CharacterPage

@Composable
fun CharacterScreen(
    state: CharacterScreenState,
    onClick: (CharacterPage) -> Unit
) {
    LazyColumn() {
        items(state.characters) { character ->
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center,
                modifier = Modifier.clickable(onClick = { onClick(character) })
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceAround
                ) {
                    AsyncImage(
                        model = character.image,
                        contentDescription = "${character.name} shown as an image.",
                        modifier = Modifier.size(100.dp)
                    )
                    Text(
                        text = character.name,
                        fontSize = 30.sp
                    )
                }
            }
        }
    }
}
