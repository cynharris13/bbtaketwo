package com.example.bbtaketwo.model.mapper.relative

import com.example.bbtaketwo.model.dtos.RelativeDTO
import com.example.bbtaketwo.model.entity.Relative
import com.example.bbtaketwo.model.mapper.Mapper

/**
 * Relative mapper changes [RelativeDTO] to [Relative].
 *
 * @constructor Create empty Relative mapper
 */
class RelativeMapper : Mapper<RelativeDTO, Relative> {
    override fun invoke(dto: RelativeDTO): Relative = with(dto) {
        Relative(
            name = name ?: "",
            relationship = relationship ?: "",
            url = url ?: "",
            wikiUrl = wikiUrl ?: ""
        )
    }
}
