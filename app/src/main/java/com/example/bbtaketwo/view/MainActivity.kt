package com.example.bbtaketwo.view

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.bbtaketwo.model.CharacterRepo
import com.example.bbtaketwo.model.entity.CharacterPage
import com.example.bbtaketwo.model.mapper.character.CharacterMapper
import com.example.bbtaketwo.model.remote.RetrofitClass
import com.example.bbtaketwo.ui.theme.BBTakeTwoTheme
import com.example.bbtaketwo.view.characterscreen.CharacterDetailsScreen
import com.example.bbtaketwo.view.characterscreen.CharacterScreen
import com.example.bbtaketwo.view.characterscreen.CharacterScreenState
import com.example.bbtaketwo.viewmodel.CharacterViewModel
import com.example.bbtaketwo.viewmodel.VMFactory

/**
 * Main activity is where the first screen is built.
 *
 * @constructor Create empty Main activity
 */
class MainActivity : ComponentActivity() {

    private val characterViewModel by viewModels<CharacterViewModel> {
        val api = RetrofitClass.getCharacterAPI()
        val repo = CharacterRepo(CharacterMapper(), api)
        VMFactory(repo)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        characterViewModel.getCharacters()
        setContent {
            val characterState by characterViewModel.characterState.collectAsState()
            BBTakeTwoTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val navController = rememberNavController()
                    NavHost(navController = navController, startDestination = "characters") {
                        composable("characters") { backStackEntry ->
                            CharacterListApp(characterState) { selected ->
                                characterViewModel.selectCharacter(selected)
                                navController.navigate("characterDetails")
                            }
                        }
                        composable("characterDetails") {
                            CharacterDetailsScreen(characterState.selectedCharacter)
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun CharacterListApp(
    state: CharacterScreenState,
    onClick: (CharacterPage) -> Unit
) {
    CharacterScreen(state, onClick)
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    BBTakeTwoTheme {
        /*
        CharacterScreen(
            CharacterScreenState(
            characters = listOf(
                CharacterPage(
                    age = "1",
                    firstEpisode = "skdjhf",
                    gender = "NB",
                    id = 1,
                    image = ""
                )
            )
        )
        )
         */
    }
}
