package com.example.bbtaketwo.model.remote.response

import com.example.bbtaketwo.model.entity.CharacterPage

/**
 * Network response.
 *
 * @param T
 * @constructor Create empty Network response
 */
sealed class NetworkResponse<T> {
    /**
     * Successful character contains the retrieved list of [CharacterPage].
     *
     * @property characters
     * @constructor Create empty Successful character
     */
    data class SuccessfulCharacter(
        val characters: List<CharacterPage>
    ) : NetworkResponse<List<CharacterPage>>()

    /**
     * Loading.
     *
     * @constructor Create empty Loading
     */
    object Loading : NetworkResponse<Unit>()

    /**
     * Error.
     *
     * @property message
     * @constructor Create empty Error
     */
    data class Error(
        val message: String
    ) : NetworkResponse<String>()
}
