package com.example.bbtaketwo.model.dtos

import kotlinx.serialization.Serializable

@Serializable
data class CharacterPageDTO(
    val age: String?,
    val firstEpisode: String?,
    val gender: String?,
    val id: Int?,
    val image: String?,
    val name: String?,
    val occupation: String?,
    val relatives: List<RelativeDTO>?,
    val url: String?,
    val voicedBy: String?,
    val wikiUrl: String?
)
