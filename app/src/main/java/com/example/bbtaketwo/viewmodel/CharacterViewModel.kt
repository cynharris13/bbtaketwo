package com.example.bbtaketwo.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.bbtaketwo.model.CharacterRepo
import com.example.bbtaketwo.model.entity.CharacterPage
import com.example.bbtaketwo.model.remote.response.NetworkResponse
import com.example.bbtaketwo.view.characterscreen.CharacterScreenState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

/**
 * Character view model.
 *
 * @property repo repository of [CharacterPage]
 * @constructor Create empty Character view model
 */
class CharacterViewModel(
    private val repo: CharacterRepo
) : ViewModel() {
    private val _characters: MutableStateFlow<CharacterScreenState> = MutableStateFlow(
        CharacterScreenState()
    )
    val characterState: StateFlow<CharacterScreenState> get() = _characters

    /**
     * Get characters.
     *
     */
    fun getCharacters(limit: Int = 30) = viewModelScope.launch {
        _characters.update { it.copy(isLoading = true) }
        when (val characters = repo.getCharacterPages(limit)) {
            is NetworkResponse.Error -> _characters.update {
                it.copy(isLoading = false, error = "oh no")
            }
            is NetworkResponse.SuccessfulCharacter -> _characters.update {
                it.copy(isLoading = false, characters = characters.characters)
            }
            else -> Log.e("CharacterViewModel", "getCharacters: uh oh")
        }
    }

    /**
     * Select character grabs a specific [CharacterPage] to display details of.
     *
     * @param character
     */
    fun selectCharacter(character: CharacterPage) = viewModelScope.launch {
        _characters.update { it.copy(selectedCharacter = character) }
    }
}

/**
 * View model factory.
 *
 * @property repo repository of [CharacterPage].
 * @constructor Create empty V m factory
 */
class VMFactory(
    private val repo: CharacterRepo
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return CharacterViewModel(repo) as T
    }
}
