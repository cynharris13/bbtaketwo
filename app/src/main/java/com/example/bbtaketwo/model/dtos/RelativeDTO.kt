package com.example.bbtaketwo.model.dtos

import kotlinx.serialization.Serializable

@Serializable
data class RelativeDTO(
    val name: String?,
    val relationship: String?,
    val url: String?,
    val wikiUrl: String?
)
