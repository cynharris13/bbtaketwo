package com.example.bbtaketwo.model.entity

/**
 * Character page.
 *
 * @property age
 * @property firstEpisode
 * @property gender
 * @property id
 * @property image
 * @property name
 * @property occupation
 * @property relatives
 * @property url
 * @property voicedBy
 * @property wikiUrl
 * @constructor Create empty Character page
 */
data class CharacterPage(
    val age: String = "",
    val firstEpisode: String = "",
    val gender: String = "",
    val id: Int = 0,
    val image: String = "",
    val name: String = "",
    val occupation: String = "",
    val relatives: List<Relative> = emptyList(),
    val url: String = "",
    val voicedBy: String = "",
    val wikiUrl: String = ""
)
