package com.example.bbtaketwo.viewmodel

import com.example.bbtaketwo.model.CharacterRepo
import com.example.bbtaketwo.model.CoroutinesTestExtension
import com.example.bbtaketwo.model.entity.CharacterPage
import com.example.bbtaketwo.model.remote.response.NetworkResponse
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

internal class CharacterViewModelTest {
    @OptIn(ExperimentalCoroutinesApi::class)
    @RegisterExtension
    val extension = CoroutinesTestExtension()
    private val repo = mockk<CharacterRepo>()
    private val vM: CharacterViewModel = CharacterViewModel(repo)

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    @DisplayName("Tests view model's state")
    fun testViewModel() = runTest(extension.dispatcher) {
        // given
        val expected = NetworkResponse.SuccessfulCharacter(
            listOf(CharacterPage(name = "Bob"))
        )
        coEvery { repo.getCharacterPages(30) } coAnswers { expected }
        // when
        vM.getCharacters()
        // then
        assertFalse(vM.characterState.value.isLoading)
        assertEquals(vM.characterState.value.characters, expected.characters)
    }

    @Test
    @DisplayName("Tests selecting a character")
    fun testSelectCharacter() = runTest(extension.dispatcher) {
        // given
        val expected = CharacterPage(name = "Bob")

        vM.selectCharacter(expected)

        assertEquals(expected, vM.characterState.value.selectedCharacter)
    }
}
