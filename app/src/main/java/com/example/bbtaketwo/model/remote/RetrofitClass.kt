package com.example.bbtaketwo.model.remote

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

/**
 * Local retrofit.
 *
 * @constructor Create empty Local retrofit
 */
object RetrofitClass {
    private const val VERSION = "/"
    private const val BASE_URL = "https://bobsburgers-api.herokuapp.com$VERSION"

    val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    /**
     * Get character API.
     *
     * @return
     */
    fun getCharacterAPI(): CharacterAPI = retrofit.create()
}
