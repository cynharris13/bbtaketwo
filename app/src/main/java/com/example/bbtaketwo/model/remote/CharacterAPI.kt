package com.example.bbtaketwo.model.remote

import com.example.bbtaketwo.model.remote.response.CharacterResponseFromJson
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Character a p i retrieves a [Call] from the http site.
 *
 * @constructor Create empty Character a p i
 */
interface CharacterAPI {

    @GET(CHARACTER_ENDPOINT)
    fun getCharacters(
        @Query(CHARACTERS_FETCHED) limit: Int
    ): Call<CharacterResponseFromJson>

    companion object {
        private const val CHARACTERS_FETCHED = "limit"
        private const val CHARACTER_ENDPOINT = "characters?&"
    }
}
