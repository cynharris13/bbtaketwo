package com.example.bbtaketwo.model.mapper.character

import com.example.bbtaketwo.model.dtos.CharacterPageDTO
import com.example.bbtaketwo.model.entity.CharacterPage
import com.example.bbtaketwo.model.mapper.Mapper
import com.example.bbtaketwo.model.mapper.relative.RelativeMapper

/**
 * Character mapper turns [CharacterPageDTO] to [CharacterPage].
 *
 * @constructor Create empty Character mapper
 */
class CharacterMapper : Mapper<CharacterPageDTO, CharacterPage> {
    override fun invoke(dto: CharacterPageDTO): CharacterPage = with(dto) {
        val mapper: RelativeMapper = RelativeMapper()
        CharacterPage(
            age = age ?: "",
            firstEpisode = firstEpisode ?: "",
            gender = gender ?: "",
            id = id ?: 0,
            image = image ?: "",
            name = name ?: "",
            occupation = occupation ?: "",
            relatives = relatives?.map { mapper(it) } ?: emptyList(),
            url = url ?: "",
            voicedBy = voicedBy ?: "",
            wikiUrl = wikiUrl ?: ""
        )
    }
}
